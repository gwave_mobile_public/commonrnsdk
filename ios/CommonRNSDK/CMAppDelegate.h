//
//  CMAppDelegate.h
//  CommonRNSDK
//
//  Created by yangpeng on 08/09/2023.
//  Copyright (c) 2023 yangpeng. All rights reserved.
//

@import UIKit;

@interface CMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
