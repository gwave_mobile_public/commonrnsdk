//
//  main.m
//  CommonRNSDK
//
//  Created by yangpeng on 08/09/2023.
//  Copyright (c) 2023 yangpeng. All rights reserved.
//

@import UIKit;
#import "CMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CMAppDelegate class]));
    }
}
