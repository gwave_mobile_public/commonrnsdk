//
//  EvgFeatureProbeManage.h
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/9/4.
//

#import <Foundation/Foundation.h>
#import "Evg.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^ErrorBlock)(EvgBundleErrorModel *_Nullable errorModel);

@interface EvgFeatureProbeManage : NSObject

@property (nonatomic, copy) NSString *globalConfig;

+ (instancetype)sharedInstance;

- (void)connectFeatureProbe:(NSString *)clientId customInfo:(NSDictionary *_Nullable)customInfo errorBlock:(ErrorBlock)errorBlock;

- (NSString *)getFeatureProbeKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
