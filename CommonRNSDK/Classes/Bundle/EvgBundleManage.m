//
//  EvgBundleManage.m
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/8/9.
//

#import "EvgBundleManage.h"
#import "Evg.h"
#import <SSZipArchive/SSZipArchive.h>
#import "EvgFeatureProbeManage.h"

@interface EvgBundleManage()

@property (nonatomic, strong) NSMutableDictionary *localBundles;

@end

@implementation EvgBundleManage

+ (instancetype)sharedInstance {
    static EvgBundleManage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)initErn:(NSString *)clientId customInfo:(NSDictionary *)customInfo;
{
    //初始化FeatureProbe
    [[EvgFeatureProbeManage sharedInstance] connectFeatureProbe:clientId customInfo:customInfo errorBlock:^(EvgBundleErrorModel * _Nullable errorModel) {
      if (errorModel) {
        NSLog(@"连接featureProbe失败");
      }
    }];
}

#pragma mark - launch unzip/copy
/**
 首次：沙盒没有bundles文件，则解压assert bundles.zip到沙盒bundles目录，创建本地版本配置表localbundles，遍历bundles信息并写入localbundles
 再次启动：沙盒bundles.json和assert bundles.json是否相等，或者本地localbundles配置表是否为空，不相等或者为空，重新执行步骤1
 覆盖安装：bundles.json不相等，则需要清空bundles目录，及本地localbundles配置表，重新执行步骤1
 */
- (EvgBundleErrorModel *)unzipBundles
{
    //bundles中如果没有bundles.json文件，则不进行copy操作
    if ([self bundlesVersion] == nil) {
        return [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeCopyFail errorMsg:nil];
    }
    //如果bundles.zip版本相同，则不进行解压
    if ([[self bundlesVersion] isEqualToString:[self bundlesDirVersion]]) {
        self.localBundles = [self getLocalBundles:KLocalBundlesInfo];
        return nil;
    }
    
    if (self.localBundles.count > 0) {
        return nil;
    }
    
    //首次安装解压到bundles目录
    EvgBundleErrorModel *errorModel = [self unZipBundlesToSandbox];
    //初始化loacalBundles对象,版本覆盖清空本地loacalBundles对象
    [self initLocalBundlesInfo];
    return errorModel;
}

//解压工作目录bundles到指定沙盒目录
- (EvgBundleErrorModel *)unZipBundlesToSandbox
{
    NSString *bundleDir = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:kBundlesDirName];
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:bundlesDirPath]) {
        [fileManager removeItemAtPath:bundlesDirPath error:&error];
    }
    BOOL isSuccess = [self createDirectoryAtPath:bundlesDirPath];
    if (isSuccess) {
        NSArray *items = [fileManager contentsOfDirectoryAtPath:bundleDir error:&error];
        BOOL isSuccess = NO;
        for (NSString *item in items) {
            NSString *fromFullPath = [bundleDir stringByAppendingPathComponent:item];
            NSString *destFullPath = [bundlesDirPath stringByAppendingPathComponent:item];
            //如果是个json文件，需要copy到沙盒bundle目录，zip文件则需要解压到沙盒bundles目录
            if([item isEqualToString:KBundlesJsonName]) {
                [fileManager copyItemAtPath:fromFullPath toPath:destFullPath error:&error];
            } else if ([item isEqualToString:[NSString stringWithFormat:@"%@.zip",kBundlesDirName]]) {
                isSuccess = [self unzipBundle:fromFullPath unZipPath:[self documentsFilePath]];
            }
        }
        if (!isSuccess || error) {
            return [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeUnZipFail errorMsg:error.localizedDescription];
        }
    }
    return nil;
}

- (void)initLocalBundlesInfo
{
    //遍历解压后的bundles目录，将config.json信息，写入到localBundles中
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *items = [fileManager contentsOfDirectoryAtPath:bundlesDirPath error:&error];
    for (NSString *item in items) {
        NSString *bundleFilePath = [bundlesDirPath stringByAppendingPathComponent:item];
        NSArray *bundleItems = [fileManager contentsOfDirectoryAtPath:bundleFilePath error:&error];
        for (NSString *bundleItem in bundleItems) {
            NSString *bundlePath = [bundleFilePath stringByAppendingPathComponent:bundleItem];
            BOOL isTempDirectory;
            if ([fileManager fileExistsAtPath:bundlePath isDirectory:&isTempDirectory] && isTempDirectory) {
                NSDictionary *localBundleDict = [self createLocalBundleInfo:bundleFilePath bundleSubPath:bundleItem];
                if (localBundleDict) {
                    [self.localBundles setObject:localBundleDict forKey:item];
                }
            }
        }
    }
    [self addLocalBundles];
}

-  (void)addLocalBundles
{
    if (self.localBundles.count > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:self.localBundles forKey:KLocalBundlesInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSLog(@"localBundles = %@",[[NSUserDefaults standardUserDefaults] dictionaryForKey:KLocalBundlesInfo]);
}

- (NSMutableDictionary *)getLocalBundles:(NSString *)bundleKey
{
    NSDictionary *localBundleDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:bundleKey];
    return localBundleDict.mutableCopy;
}

- (NSString *)bundlesVersion
{
    //读取工程bundles目录json
    NSString *bundleDir = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:kBundlesDirName];
    NSString *jsonPath = [bundleDir stringByAppendingPathComponent:KBundlesJsonName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:jsonPath]) {
        NSDictionary *bundleJsonDict = [self jsonToDict:jsonPath];
        NSString *bundleVersion = [bundleJsonDict valueForKey:@"version"];
        return bundleVersion;
    }
    return nil;
}

- (NSString *)bundlesDirVersion
{
    //读取沙盒bundles目录json
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSString *jsonDirPath = [bundlesDirPath stringByAppendingPathComponent:KBundlesJsonName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:jsonDirPath]) {
        NSDictionary *bundleJsonDict = [self jsonToDict:jsonDirPath];
        NSString *bundleDirVersion = [bundleJsonDict valueForKey:@"version"];
        return bundleDirVersion;
    }
    return nil;
}

#pragma mark - load bundle
/**
 1、需要回滚（rollback包含currentVersion），远端配置版本本地存在，则直接加载
 2、需要回滚（rollback包含currentVersion），远端配置版本本地不存在，则返回空
 3、需要回滚（rollback包含currentVersion），稳定版本不在rollback，则直接加载稳定版本
 4、远端配置版本本地存在，找到后直接加载
 5、远端配置版本本地不存在(首次下载此bundle)，则下载最新版本后直接加载
 6、远端配置版本和unstableVersion相等，本地存在稳定版本，则直接加载
 7、远端配置版本和unstableVersion相等，本地不存在稳定版本，则返回空
 8、远端配置版本和unstableVersion不相等，远端配置版本本地不存在，需要实时生效，则下载最新版本后直接加载 1
 9、远端配置版本和unstableVersion不相等，远端配置版本本地不存在，不需要实时生效，则直接加载稳定版本，同步下载最新版本后，第二次加载远端配置版本
 10、指定版本配置为稳定版本
 11、指定版本配置为不稳定版本
 12、文件数量大于KBundleMaxCount，需要清空创建时间最早的一个
*/

- (void)getBundleInfo:(NSString *)bundleName bundleInfo:(BundleInfoBlock)bundleInfoBlock
{
    __weak typeof(self) weakSelf = self;
    [self getBundlesBundle:bundleName bundleInfo:^(EvgBundleModel * _Nullable bundleModel, EvgBundleErrorModel *_Nullable errorModel) {
        //写入self.localBundles
        if (bundleModel) {
            [weakSelf updateLocalBundlesVersion:bundleModel];
        }
        bundleInfoBlock(bundleModel, errorModel);
    }];
}

- (void)updateLocalBundlesVersion:(EvgBundleModel *)bundleModel
{
    NSMutableDictionary *bundleInfo = [[self.localBundles valueForKey:bundleModel.businessName] mutableCopy];
    if (bundleInfo.count > 0) {
        if (![[bundleInfo valueForKey:@"currentVersion"] isEqualToString:bundleModel.version]) {
            [bundleInfo setObject:bundleModel.version forKey:@"currentVersion"];
            [self.localBundles setObject:bundleInfo forKey:bundleModel.businessName];
            [self addLocalBundles];
        }
    } else {
        NSMutableDictionary *localBundlesDict = [[NSMutableDictionary alloc] init];
        [localBundlesDict setObject:@{} forKey:@"stable"];
        [localBundlesDict setObject:bundleModel.bundleSubPath forKey:@"bundleSubPath"];
        [localBundlesDict setObject:@"" forKey:@"unStableVersion"];
        [localBundlesDict setObject:bundleModel.version forKey:@"currentVersion"];
        [self.localBundles setObject:localBundlesDict forKey:bundleModel.businessName];
        [self addLocalBundles];
    }
}

- (void)getBundlesBundle:(NSString *)bundleName bundleInfo:(BundleInfoBlock)bundleInfoBlock
{
    if (bundleName.length <= 0) {
        bundleInfoBlock(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeBundleNameNull errorMsg:nil]);
        return;
    }
    
    //读取对应bundle远端配置
    NSString *remoteBundleStr = [[EvgFeatureProbeManage sharedInstance] getFeatureProbeKey:bundleName];
    NSDictionary *remoteBundlesInfo = [self jsonStringToDict:remoteBundleStr];
    NSLog(@"%@ === %@",bundleName, remoteBundlesInfo);
    NSString *remoteVersion = [remoteBundlesInfo objectForKey:@"version"];
    BOOL currentEffect = [[remoteBundlesInfo objectForKey:@"currentEffect"] boolValue]; //下载完后立即生效
    
    NSString *rollbackStr = [[EvgFeatureProbeManage sharedInstance] globalConfig];
    NSDictionary *globalConfig = [self jsonStringToDict:rollbackStr];
    NSDictionary *rollbackDict = [globalConfig objectForKey:@"rollback"];
    NSArray *rollback = [rollbackDict objectForKey:bundleName];
    NSLog(@"globalConfig === %@", globalConfig);

    //本地localBundle配置表
    if (self.localBundles.count == 0) {
        bundleInfoBlock(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeBundlesJsonNotExist errorMsg:nil]);
        return;
    }
    
    NSDictionary *localBundleInfo = [self.localBundles valueForKey:bundleName];
    NSDictionary *stableBundleInfo = [localBundleInfo valueForKey:@"stable"];
    NSString *currentVersion = [localBundleInfo valueForKey:@"currentVersion"];
    NSString *unStableVersion = [localBundleInfo valueForKey:@"unStableVersion"];//loadfail之后标记为不稳定版本

    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSString *bundlePath = [bundlesDirPath stringByAppendingPathComponent:bundleName];

    //rollback包含正在使用的版本
    if (rollback && [rollback containsObject:currentVersion]){
        //稳定版本不在rollback
        if(stableBundleInfo.count > 0 && ![rollback containsObject:[stableBundleInfo valueForKey:@"version"]]){
            //返回稳定版本
            NSString *bundleSubPath = [localBundleInfo valueForKey:@"bundleSubPath"];
            NSString *bundleItemPath = [bundlePath stringByAppendingPathComponent:bundleSubPath];
            bundleInfoBlock([self createBundleModel:bundleItemPath], nil);
        } else {
            //查找remoteVersion本地是否存在
            NSString *bundleItemPath = [self bundleExists:bundleName bundleVersion:remoteVersion];
            if (bundleItemPath.length > 0) {
                bundleInfoBlock([self createBundleModel:bundleItemPath], nil);
            } else {
                bundleInfoBlock(nil, nil);
            }
        }
    } else {
        NSString *bundleItemPath = [self bundleExists:bundleName bundleVersion:remoteVersion];
        if (bundleItemPath.length > 0) {
            bundleInfoBlock([self createBundleModel:bundleItemPath], nil);
        } else {
            if ([unStableVersion isEqualToString:remoteVersion]) {
                if (stableBundleInfo.count > 0) {
                    //返回稳定版本
                    NSString *bundleSubPath = [localBundleInfo valueForKey:@"bundleSubPath"];
                    NSString *bundleItemPath = [bundlePath stringByAppendingPathComponent:bundleSubPath];
                    bundleInfoBlock([self createBundleModel:bundleItemPath], nil);
                } else {
                    bundleInfoBlock(nil, nil);
                }
            } else {
                NSDictionary *localBundleInfo = [self.localBundles valueForKey:bundleName];
                if (currentEffect || localBundleInfo.count == 0) {
                    //下载立即生效 或者本地不存在bundle
                    if (remoteVersion.length > 0) {
                        __weak typeof(self) weakSelf = self;
                        [self downloadBundle:remoteBundlesInfo downloadPathBlock:^(NSString *downloadPath, EvgBundleErrorModel *errorModel) {
                            if (downloadPath.length > 0) {
                                bundleInfoBlock([weakSelf createBundleModel:downloadPath], nil);
                            } else {
                                bundleInfoBlock(nil, errorModel);
                            }
                        }];
                    } else {
                        bundleInfoBlock(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeRequestFeatureProbeException errorMsg:bundleName]);
                    }
                } else {
                    NSString *bundleSubPath = [localBundleInfo valueForKey:@"bundleSubPath"];
                    NSString *bundleItemPath = [bundlePath stringByAppendingPathComponent:bundleSubPath];
                    bundleInfoBlock([self createBundleModel:bundleItemPath], nil);
                    //下载解压bundle
                    if (remoteVersion.length > 0) {
                        [self downloadBundle:remoteBundlesInfo downloadPathBlock:^(NSString *downloadPath, EvgBundleErrorModel *errorModel) {}];
                    }
                }
            }
        }
    }
}

- (NSString *)bundleExists:(NSString *)bundleName bundleVersion:(NSString *)bundleVersion
{
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSString *bundlePath = [bundlesDirPath stringByAppendingPathComponent:bundleName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *bundleItemPath;
    if ([fileManager fileExistsAtPath:bundlePath]) {
        NSError *error = nil;
        NSArray *items = [fileManager contentsOfDirectoryAtPath:bundlePath error:&error];
        for (NSString *item in items) {
            bundleItemPath = [bundlePath stringByAppendingPathComponent:item];
            BOOL isTempDirectory;
            if ([fileManager fileExistsAtPath:bundleItemPath isDirectory:&isTempDirectory] && isTempDirectory) {
                NSString *configJsonPath = [bundleItemPath stringByAppendingPathComponent:KBundleConfigName];
                NSDictionary *configJsonDict = [self jsonToDict:configJsonPath];
                NSString *configVersion = [configJsonDict valueForKey:@"version"];
                if ([configVersion isEqualToString:bundleVersion]){
                    return bundleItemPath;
                }
            }
        }
    }
    return nil;
}

- (NSDictionary *)createLocalBundleInfo:(NSString *)bundlePath bundleSubPath:(NSString *)bundleSubPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableDictionary *localBundlesDict = [[NSMutableDictionary alloc] init];
    
    NSString *bundleItemPath = [bundlePath stringByAppendingPathComponent:bundleSubPath];
    
    if ([fileManager fileExistsAtPath:bundleItemPath]) {
        NSString *configJsonPath = [bundleItemPath stringByAppendingPathComponent:KBundleConfigName];
        NSDictionary *configJsonDict = [self jsonToDict:configJsonPath];
        [localBundlesDict setObject:configJsonDict forKey:@"stable"];
        [localBundlesDict setObject:bundleSubPath forKey:@"bundleSubPath"];
        [localBundlesDict setObject:@"" forKey:@"unStableVersion"];
        [localBundlesDict setObject:[configJsonDict valueForKey:@"version"] forKey:@"currentVersion"];
    }
    return localBundlesDict.copy;
}

//在指定的路径下构造EvgBundleModel对象
- (EvgBundleModel *)createBundleModel:(NSString *)bundlePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:bundlePath]) {
        NSString *configJsonPath = [bundlePath stringByAppendingPathComponent:KBundleConfigName];
        NSDictionary *configJsonDict = [self jsonToDict:configJsonPath];
        if (configJsonDict.count > 0) {
            EvgBundleModel *bundleModel = [[EvgBundleModel alloc] init];
            bundleModel.bundleUrl = [NSURL URLWithString:[bundlePath stringByAppendingPathComponent:[configJsonDict valueForKey:@"indexFileName"]]];
            bundleModel.entryName = [configJsonDict valueForKey:@"entryName"];
            bundleModel.version = [configJsonDict valueForKey:@"version"];
            bundleModel.businessName = [configJsonDict valueForKey:@"businessName"];
            bundleModel.indexFileName = [configJsonDict valueForKey:@"indexFileName"];
            bundleModel.bundleSubPath = [bundlePath lastPathComponent];
            return bundleModel;
        }
    }
    return nil;
}

//bundle.Zip下载
- (void)downloadBundle:(NSDictionary *)remoteBundlesInfo downloadPathBlock:(void(^)(NSString *downloadPath, EvgBundleErrorModel *_Nullable errorModel))downloadPath
{
    NSString *remoteVersion = [remoteBundlesInfo objectForKey:@"version"];
    NSString *bundleName = [remoteBundlesInfo objectForKey:@"businessName"];
    NSString *url = [remoteBundlesInfo objectForKey:@"remoteZipUrl"];
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSString *bundlePath = [bundlesDirPath stringByAppendingPathComponent:bundleName];
    //zip文件写入到该文件夹
    BOOL isSuccess = [self createDirectoryAtPath:bundlePath];
    if (!isSuccess) {
        return;
    }

    NSString *tempfileName = [NSString stringWithFormat:@"%@%@",bundleName,remoteVersion];
    NSString *zipfileName = [NSString stringWithFormat:@"%@.zip",tempfileName];
    NSString *zipBundlePath = [bundlePath stringByAppendingPathComponent:zipfileName];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURL *downloadUrl = [NSURL URLWithString:url];
    __weak typeof(self) weakSelf = self;
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:downloadUrl completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"下载失败： %@",error);
            downloadPath(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeDownLoadFail errorMsg:error.localizedDescription]);
            return;
        }
        NSString *locationPath = location.path;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *moveError = nil;
        if ([fileManager fileExistsAtPath:zipBundlePath]) {
            [fileManager removeItemAtPath:zipBundlePath error:&moveError];
        }
        [weakSelf removeBundleIfNeed:bundleName];
        [fileManager moveItemAtPath:locationPath toPath:zipBundlePath error:&moveError];

        //解压
        if (moveError == nil) {
            BOOL isSuccess = [weakSelf unzipBundle:zipBundlePath unZipPath:bundlePath];
            if (isSuccess) {
                [fileManager removeItemAtPath:zipBundlePath error:&moveError];
                NSLog(@"下载成功，解压成功");
                downloadPath([bundlePath stringByAppendingPathComponent:tempfileName], nil);
            } else {
                NSLog(@"下载成功，解压失败");
                downloadPath(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeUnZipFail errorMsg:nil]);
            }
        } else {
            NSLog(@"下载失败");
            downloadPath(nil, [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeDownLoadFail errorMsg:moveError.localizedDescription]);
        }
    }];
    [downloadTask resume];
}

- (void)removeBundleIfNeed:(NSString *)bundleName
{
    //检测到容量大于KBundleMaxCount时，删除bundle文件夹内创建时间最早的bundle
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSString *bundlePath = [bundlesDirPath stringByAppendingPathComponent:bundleName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:bundlePath]) {
        NSArray *items = [fileManager contentsOfDirectoryAtPath:bundlePath error:&error];
        NSMutableArray *fileArray = [[NSMutableArray alloc] init];
        for (NSString *item in items) {
            NSString *bundleItemPath = [bundlePath stringByAppendingPathComponent:item];
            BOOL isDirectory;
            if([fileManager fileExistsAtPath:bundleItemPath isDirectory:&isDirectory] && isDirectory) {
                [fileArray addObject:bundleItemPath];
            }
        }
        if (fileArray.count < KBundleMaxCount){
            return;
        }
        NSDate *earliestCreationDate = [NSDate distantFuture];
        NSString *earliestFilePath = nil;
        for (int i = 0; i < fileArray.count; i++) {
            NSDictionary *attributes = [fileManager attributesOfItemAtPath:[fileArray objectAtIndex:i] error:&error];
            if (attributes) {
                NSDate *createData = attributes[NSFileCreationDate];
                if ([createData compare:earliestCreationDate] == NSOrderedAscending) {
                    earliestCreationDate = createData;
                    earliestFilePath = [fileArray objectAtIndex:i];
                }
            }
        }
        if (earliestFilePath.length > 0) {
            if ([fileManager fileExistsAtPath:earliestFilePath]) {
                [fileManager removeItemAtPath:earliestFilePath error:&error];
            }
        }
    }
}

//读取指定bundleName.jsbundle信息
- (EvgBundleContentModel *)getBundleContent:(NSString *)bundlePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    EvgBundleContentModel *contentModel = [[EvgBundleContentModel alloc] init];
    if ([fileManager fileExistsAtPath:bundlePath]) {
        //获取文件上一级
        NSString *bundleItemPath = [bundlePath stringByDeletingLastPathComponent];
        NSString *configJsonPath = [bundleItemPath stringByAppendingPathComponent:KBundleConfigName];
        NSDictionary *configJsonDict = [self jsonToDict:configJsonPath];
        NSString *fileMD5 = [configJsonDict objectForKey:@"fileMD5"];
        //生成bundlePath的MD5值
        NSString *bundleMD5 = [EvgFileMd5 fileMD5:bundlePath];
        if ([fileMD5 isEqualToString:bundleMD5]) {
            //读取jsbundle内容并返回
            NSString *bundleContent = [NSString stringWithContentsOfFile:bundlePath encoding:NSUTF8StringEncoding error:&error];
            if (error == nil) {
                contentModel.bundleContent = bundleContent;
                return contentModel;
            }
        } else {
            contentModel.errorModel = [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeVerifyMd5Fail errorMsg:nil];
            return contentModel;
        }
    }
    contentModel.errorModel = [EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeReadFail errorMsg:nil];
    return contentModel;
}
    
//清空本地临时变量
- (void)removeLocalBundleInfo
{
    //清空本地存储
    [self.localBundles removeAllObjects];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KLocalBundlesInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //清空沙盒目录
    NSString *bundlesDirPath = [self getFilePath:kBundlesDirName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:bundlesDirPath]) {
        [fileManager removeItemAtPath:bundlesDirPath error:&error];
    }
}

/**
 1、js通知为稳定版本，则将此版本写入配置表localBundles
 2、js通知为不稳定版本，则在配置表中将此版本赋值给unStableVersion
 */
#pragma mark - sign bundle

//标记为稳定版本
- (void)signStableVersion:(NSString *)bundleName bundleVersion:(NSString *)bundleVersion
{
    //查找本地bundle，将指定版本bundle写入localBundles
    NSMutableDictionary *localBundleDict = [[self.localBundles valueForKey:bundleName] mutableCopy];
    NSDictionary *stableDict = [localBundleDict objectForKey:@"stable"];
    if ([[stableDict objectForKey:@"version"] isEqualToString:bundleVersion]) {
        return;
    }
    NSString *bundleItemPath = [self bundleExists:bundleName bundleVersion:bundleVersion];
    if(bundleItemPath.length > 0) {
        NSString *configJsonPath = [bundleItemPath stringByAppendingPathComponent:KBundleConfigName];
        NSDictionary *configJsonDict = [self jsonToDict:configJsonPath];
        [localBundleDict setObject:configJsonDict forKey:@"stable"];
        [localBundleDict setObject:@"" forKey:@"unStableVersion"];
        [self.localBundles setObject:localBundleDict forKey:bundleName];
        [self addLocalBundles];
    }
}

//标记为不稳定版本
- (void)signUnstableVersion:(NSString *)bundleName bundleVersion:(NSString *)bundleVersion
{
    //移除当前版本，并标记unstableVersion字段
    NSString *bundleItemPath = [self bundleExists:bundleName bundleVersion:bundleVersion];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:bundleItemPath]) {
        [fileManager removeItemAtPath:bundleItemPath error:&error];
    }
    NSMutableDictionary *bundleInfo = [[self.localBundles valueForKey:bundleName] mutableCopy];
    [bundleInfo setObject:bundleVersion forKey:@"unStableVersion"];
    NSDictionary *stableDict = [bundleInfo valueForKey:@"stable"];
    if ([bundleVersion isEqualToString:[stableDict valueForKey:@"version"]]) {
        [bundleInfo setObject:@{} forKey:@"stable"];
    }
    [self.localBundles setObject:bundleInfo forKey:bundleName];
    [self addLocalBundles];
}

#pragma mark - public function

//zip文件解压
- (BOOL)unzipBundle:(NSString *)zipPath unZipPath:(NSString *)unzipPath
{
    if (zipPath.length == 0 || unzipPath.length == 0) {
        return NO;
    }
    return [SSZipArchive unzipFileAtPath:zipPath toDestination:unzipPath];
}

- (NSDictionary *)jsonToDict:(NSString *)jsonPath
{
    NSError *error = nil;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath options:0 error:&error];
    if (jsonData == nil) {
        NSLog(@"无法读取 JSON 文件：%@", error.localizedDescription);
        return nil;
    }

    // 解析 JSON 数据
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (jsonObject == nil) {
        NSLog(@"无法解析 JSON 文件：%@", error.localizedDescription);
        return nil;
    }
    return jsonObject;
}

- (NSDictionary *)jsonStringToDict:(NSString *)jsonStr
{
    if (jsonStr.length == 0) {
        return nil;
    }
    NSError *error = nil;
    NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    if (jsonObject == nil) {
        return nil;
    }
    return jsonObject;
}

//获取沙盒文件路径
- (NSString *)documentsFilePath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

//获取沙盒目录文件路径
- (NSString *)getFilePath:(NSString *)fileName
{
    return [[self documentsFilePath] stringByAppendingFormat:@"/%@/",fileName];
}

//指定路径，文件创建
- (BOOL)createDirectoryAtPath:(NSString *)filePath
{
    if (filePath.length == 0) {
        return NO;
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:NULL error:&error];
    }
    if (error) {
        NSLog(@"file create fail-%@",error.localizedDescription);
        return NO;
    }
    return YES;
}

- (NSMutableDictionary *)localBundles
{
    if (!_localBundles) {
        _localBundles = [[NSMutableDictionary alloc] init];
    }
    return _localBundles;
}

@end
