//
//  EvgBundleModel.h
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/8/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, EvgBundleErrorType) {
    EvgBundleErrorTypeCopyFail = 100,
    EvgBundleErrorTypeUnZipFail = 101,
    EvgBundleErrorTypeVerifyMd5Fail = 102,
    EvgBundleErrorTypeDownLoadFail = 103,
    EvgBundleErrorTypeReadFail = 104,
    EvgBundleErrorTypeRequestFeatureProbeException = 105,
    EvgBundleErrorTypeBundlesJsonNotExist = 106,
    EvgBundleErrorTypeBundleNameNull = 107,
    EvgBundleErrorTypeUnknown = -1
};

@interface EvgBundleModel : NSObject

@property (nonatomic, strong) NSURL *bundleUrl;
@property (nonatomic, copy) NSString *entryName;
@property (nonatomic, copy) NSString *bundleSubPath;
@property (nonatomic, copy) NSString *businessName;
@property (nonatomic, copy) NSString *indexFileName;
@property (nonatomic, copy) NSString *version;

@end

@interface EvgBundleErrorModel : NSObject

@property (nonatomic, assign) EvgBundleErrorType errorCode;
@property (nonatomic, copy) NSString *errorMsg;

+ (EvgBundleErrorModel *)createBundleErrorModel:(EvgBundleErrorType)errorCode errorMsg:(NSString *_Nullable)errorMsg;

@end

@interface EvgBundleContentModel : NSObject

@property (nonatomic, strong) EvgBundleErrorModel *errorModel;
@property (nonatomic, copy) NSString *bundleContent;

@end
NS_ASSUME_NONNULL_END
