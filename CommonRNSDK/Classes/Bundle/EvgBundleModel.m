//
//  EvgBundleModel.m
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/8/10.
//

#import "EvgBundleModel.h"

@implementation EvgBundleModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end

@implementation EvgBundleErrorModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

+ (EvgBundleErrorModel *)createBundleErrorModel:(EvgBundleErrorType)errorCode errorMsg:(NSString *_Nullable)errorMsg
{
    EvgBundleErrorModel *errorModel = [[EvgBundleErrorModel alloc] init];
    errorModel.errorCode = errorCode;
    if (errorMsg.length > 0) {
        if (errorCode == EvgBundleErrorTypeRequestFeatureProbeException) {
            errorModel.errorMsg = [NSString stringWithFormat:@"ern request featureProbe %@ null {}", errorMsg];
        } else {
            errorModel.errorMsg = errorMsg;
        }
    } else {
        errorModel.errorMsg = [self bundleErrorMsg:errorCode];
    }
    return errorModel;
}

+ (NSString *)bundleErrorMsg:(EvgBundleErrorType)errorType
{
    switch (errorType) {
        case EvgBundleErrorTypeUnknown:
            return @"unknownc error";
            break;
        case EvgBundleErrorTypeDownLoadFail:
            return @"ern download exception";
            break;
        case EvgBundleErrorTypeUnZipFail:
            return @"ern unzip file exception";
            break;
        case EvgBundleErrorTypeReadFail:
            return @"ern read bundle content exception";
            break;
        case EvgBundleErrorTypeCopyFail:
            return @"ern copy exception";
            break;
        case EvgBundleErrorTypeVerifyMd5Fail:
            return @"ern verify md5 exception";
            break;
        case EvgBundleErrorTypeRequestFeatureProbeException:
            return @"ern request featureProbe bundleName null {}";
            break;
        case EvgBundleErrorTypeBundlesJsonNotExist:
            return @"bundles.json file is not exist";
            break;
        case EvgBundleErrorTypeBundleNameNull:
            return @"loading local bundle fail, bundleName is nil";
            break;
        default:
            return @"unknownc error";
            break;
    }
}

@end

@implementation EvgBundleContentModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
