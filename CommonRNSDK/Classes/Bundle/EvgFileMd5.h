//
//  EvgFileMd5.h
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/9/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EvgFileMd5 : NSObject

+ (NSString *)fileMD5:(NSString *)filePath;

@end

NS_ASSUME_NONNULL_END
