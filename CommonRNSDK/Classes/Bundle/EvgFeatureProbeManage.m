//
//  EvgFeatureProbeManage.m
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/9/4.
//

#import "EvgFeatureProbeManage.h"
#import "EvgBundleManage.h"
@import FeatureProbe;

@interface EvgFeatureProbeManage()

@property (nonatomic, strong) FeatureProbe *featureProbe;//存储远端配置数据

@end

@implementation EvgFeatureProbeManage

+ (instancetype)sharedInstance {
    static EvgFeatureProbeManage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)connectFeatureProbe:(NSString *)clientId customInfo:(NSDictionary *_Nullable)customInfo errorBlock:(ErrorBlock)errorBlock
{
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *urlStr = @"https://server.featureprobe.com";
    FpUrl *url = [[[FpUrlBuilder alloc] initWithRemoteUrl: urlStr] build];
    FpUser *user = [[FpUser alloc] init];
    [user withKey:@"version" value:appVersion];
    [user withKey:@"platform" value:@"2"];
    [user withKey:@"opened" value:@"1"];
    if (customInfo.count > 0) {
        [customInfo enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [user withKey:key value:obj];
        }];
    }
    FpConfig *config = [[FpConfig alloc] initWithRemoteUrl:url
                                              clientSdkKey:clientId
                                           refreshInterval:10
                                                 startWait:2];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        FeatureProbe *fp = [[FeatureProbe alloc] initWithConfig:config user:user];
        self.featureProbe = fp;
        self.globalConfig = [self getFeatureProbeKey:@"globalConfig"];
        if ([self.globalConfig isEqualToString:@"{}"]) {
            errorBlock([EvgBundleErrorModel createBundleErrorModel:EvgBundleErrorTypeRequestFeatureProbeException errorMsg:@"globalConfig"]);
        } else {
            errorBlock(nil);
        }
    });
}

- (NSString *)getFeatureProbeKey:(NSString *)key
{
    if (self.featureProbe) {
        NSString *value = [self.featureProbe jsonValueWithKey:key defaultValue: @"{}"];
        return value;
    }
    return nil;
}

@end
