//
//  EvgFileMd5.m
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/9/6.
//

#import "EvgFileMd5.h"
#import <CommonCrypto/CommonDigest.h>

@implementation EvgFileMd5

+ (NSString *)fileMD5:(NSString *)filePath
{
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:filePath];
    if (handle == nil) {
        return nil;
    }
    
    CC_MD5_CTX md5Context;
    CC_MD5_Init(&md5Context);
    
    NSData *fileData;
    do {
        fileData = [handle readDataOfLength:4096];
        CC_MD5_Update(&md5Context, fileData.bytes, (CC_LONG)fileData.length);
    } while (fileData.length > 0);
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5Context);
    
    NSMutableString *md5String = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [md5String appendFormat:@"%02x", digest[i]];
    }
    
    return [md5String copy];
}

@end
