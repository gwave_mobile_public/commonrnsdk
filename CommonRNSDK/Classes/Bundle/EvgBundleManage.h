//
//  EvgBundleManage.h
//  CommonRNSDK
//
//  Created by 杨鹏 on 2023/8/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class EvgBundleModel;
@class EvgBundleErrorModel;
@class EvgBundleContentModel;

//Bundles目录
#define kBundlesDirName @"bundles"

//bundles配置文件
#define KBundlesJsonName @"bundles.json"

//本地存储的bundle信息
#define KLocalBundlesInfo @"LocalBundles"

//bundle配置文件
#define KBundleConfigName @"config.json"

//bundel文件最大容量
#define KBundleMaxCount 5

typedef void(^BundleInfoBlock)(EvgBundleModel  *_Nullable bundleModel, EvgBundleErrorModel *_Nullable errorModel);

@interface EvgBundleManage : NSObject

+ (instancetype)sharedInstance;

/*
 工作目录bundles层级
     bundles.zip
     bundles.json
 
 沙盒目录层级参考：
 bundles
    common
        common100
        common101
        common102.zip
    kktrade
        kktrade100
        kktrade101
    bundles.json

 */

//初始化参数
- (void)initErn:(NSString *)clientId customInfo:(NSDictionary *)customInfo;

//解压bundles到沙盒目录
- (EvgBundleErrorModel *)unzipBundles;

//加载指定bundleName
- (void)getBundleInfo:(NSString *)bundleName bundleInfo:(BundleInfoBlock)bundleInfoBlock;

//标记为稳定版本
- (void)signStableVersion:(NSString *)bundleName bundleVersion:(NSString *)bundleVersion;

//标记为不稳定版本
- (void)signUnstableVersion:(NSString *)bundleName bundleVersion:(NSString *)bundleVersion;

//根据路径获取bundleName.jsbundle信息
- (EvgBundleContentModel *)getBundleContent:(NSString *)bundlePath;

//清空本地临时变量
- (void)removeLocalBundleInfo;

@end

NS_ASSUME_NONNULL_END
