//
//  NavigationBridge.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/11.
//

#import <Foundation/Foundation.h>
#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#else
#import "RCTBridge.h"
#endif

NS_ASSUME_NONNULL_BEGIN

@interface EvgNavigationModule : NSObject<RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
