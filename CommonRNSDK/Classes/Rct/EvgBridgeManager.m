//
//  EvgBridgeManager.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/7.
//

#import "EvgBridgeManager.h"
#import "RCTBridge+EVG.h"
#import "EvgBridge.h"
#import "EvgBundleManage.h"
#import "EvgBundleModel.h"

NSString *const EvgBridgeManagerJavaScriptStartNotification = @"EvgBridgeManagerJavaScriptStartNotification";
NSString *const EvgBridgeManagerJavaScriptDidLoadNotification = @"EvgBridgeManagerJavaScriptDidLoadNotification";
NSString *const EvgBridgeManagerBusinessJavaScriptDidLoadNotification = @"EvgBridgeManagerBusinessJavaScriptDidLoadNotification";
NSString *const EvgBridgeManagerJavaScriptDidFailNotification = @"EvgBridgeManagerJavaScriptDidFailNotification";

@interface EvgBridgeManager()

@property (nonatomic, strong) RCTBridge *bridge;
@property (nonatomic, copy) NSString *commonBundleName;
@property (nonatomic, strong) NSMutableDictionary *loadedBundles;

@property (nonatomic, assign) BOOL isCommonLoaded;

@end

@implementation EvgBridgeManager

static id instance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.commonBundleName = @"common";
  }
  return self;
}

#pragma mark -- bundle路径url

- (NSURL *)sourceURLWithBundleName:(NSString *)bundleName
                        bundlePath:(NSString *)bundlePath {
  
  return  [[NSBundle mainBundle] URLForResource:bundleName
                                  withExtension:@"jsbundle"
                                   subdirectory:bundlePath];
}

- (NSURL *)commonSourceUrl {
    return [self sourceURLWithBundleName:@"index.ios" bundlePath:@"bundles/common"];
}

#pragma mark -- 初始化bridge
- (void)initBridgeManagerWithOptions:(NSDictionary *)launchOptions
                       SuccessHandle:(SuccessBlock)success
                        FailedHandle:(FailedBlock)failed {
    // 获取common包的url
    NSURL *commonUrl = [self commonSourceUrl];
    // 加载common包
    [self initBridgeManagerWithBundleUrl:commonUrl
                                 Options:launchOptions
                           SuccessHandle:success
                            FailedHandle:failed];
  
}

- (void)initBridgeManagerWithBundleUrl:(NSURL *)bundleUrl
                               Options:(NSDictionary *)launchOptions
                         SuccessHandle:(SuccessBlock)success
                          FailedHandle:(FailedBlock)failed {
  if (bundleUrl == nil) {
    !failed ?: failed(@"common bundle url is empty, please checkout common bundle in sandbox");
    return;
  }
  RCTBridge *bridge = nil;
#if DEBUG
  bridge = [[RCTBridge alloc] initWithDelegate:self bundleURL:bundleUrl moduleProvider:nil launchOptions:launchOptions];
#else
  bridge = [[RCTBridge alloc] initWithBundleURL:bundleUrl moduleProvider:nil launchOptions:launchOptions];
#endif
  
  [self initBridgeManagerWith:bridge SuccessHandle:success FailedHandle:failed];
}

- (void)initBridgeManagerWith:(RCTBridge *)bridge
                SuccessHandle:(SuccessBlock)success
                 FailedHandle:(FailedBlock)failed {
  if (self.bridge != nil) {
    // 只初始化一次
    !failed ?: failed(@"bridge has been initialed");
    return;
  }
  
  self.bridge = bridge;
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(javaScriptDidLoaded:) name:RCTJavaScriptDidLoadNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willStartExecuting:) name:RCTJavaScriptWillStartExecutingNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(javaScriptDidFail:) name:RCTJavaScriptDidFailToLoadNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bridgeWillReload:) name:RCTBridgeWillReloadNotification object:nil];
  
  !success ?: success();
}


#pragma mark -- 加载模块的ReactNative根视图
- (void)loadJavaScriptWithBundleUrl:(NSURL*)bundleUrl {
  [self loadJavaScriptWithBundleUrl:bundleUrl completed:nil];
}

/**
 * 原生加载业务包
 * 若使用JavaScript加载bundle，则不使用该方法
 */
- (void)loadJavaScriptWithBundleUrl:(NSURL*)bundleUrl completed:(void(^)(NSString *url))completed {
  
  while (!self.isCommonLoaded) {
    // 监听等待
  }
  
  BOOL isExit = [self getExitBundle:bundleUrl.path];
  if (!isExit) {
#if DEBUG
    [self.bridge.batchedBridge loadAndExecuteSplitBundleURL:bundleUrl onError:^(NSError *error) {
      !completed ?: completed(nil);
    } onComplete:^{
      !completed ?: completed(bundleUrl.path);
    }];
#else
    self.bridge.businessURL = bundleUrl;
    __weak typeof(self) weakSelf = self;
    [RCTJavaScriptLoader loadBundleAtURL:bundleUrl onProgress:nil onComplete:^(NSError *error, RCTSource *source) {
        if (source) {
            [weakSelf.bridge.batchedBridge dispatchBlock:^{
              [weakSelf.bridge.batchedBridge executeSourceCode:source.data withSourceURL:bundleUrl sync:NO];
              !completed ?: completed(bundleUrl.path);
            } queue:RCTJSThread];
        }
    }];
#endif
  } else {
    !completed ?: completed(bundleUrl.path);
  }
}

/**
 * 原生加载业务包 + 创建视图
 */
- (RCTRootView *)buildRootViewWithBundleUrl:(NSURL*)bundleUrl
                                 moduleName:(NSString *)moduleName {
  
  [self loadJavaScriptWithBundleUrl:bundleUrl];
  
  return [self buildRootViewWithModuleName:moduleName];
}

/**
 * 仅创建RN视图
 */
- (RCTRootView *)buildRootViewWithModuleName:(NSString *)moduleName {
  return [self buildRootViewWithModuleName:moduleName initialProperties:@{}];
}

- (RCTRootView *)buildRootViewWithModuleName:(NSString *)moduleName
                           initialProperties:(NSDictionary *)initialProperties {

  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:self.bridge
                                                   moduleName:moduleName
                                            initialProperties:initialProperties];
  rootView.backgroundColor = [UIColor clearColor];
  
  return rootView;
}

#pragma mark -- Notifacation
- (void)willStartExecuting:(NSNotification *)noti {
  [[NSNotificationCenter defaultCenter] postNotificationName:EvgBridgeManagerJavaScriptStartNotification object:nil];
}

- (void)javaScriptDidLoaded:(NSNotification *)noti {
  RCTBridge *bridge = noti.object;
  NSURL *bundleUrl = bridge.businessURL;
  BOOL isCommon = NO;
  if (bundleUrl == nil || bundleUrl.path.length == 0) {
    isCommon = YES;
    bundleUrl = bridge.bundleURL;
  }
  if ([self getExitBundle:bundleUrl.path]) {
    return;
  }
  
  if (isCommon) {
    self.isCommonLoaded = YES;
    [self saveBundleForKey:bundleUrl.path];
    [[NSNotificationCenter defaultCenter] postNotificationName:EvgBridgeManagerJavaScriptDidLoadNotification object:nil];
  } else {
    [self saveBundleForKey:bridge.businessURL.path];
    [[NSNotificationCenter defaultCenter] postNotificationName:EvgBridgeManagerBusinessJavaScriptDidLoadNotification object:nil];
  }
}

- (void)javaScriptDidFail:(NSNotification *)noti {
  [[NSNotificationCenter defaultCenter] postNotificationName:EvgBridgeManagerJavaScriptDidFailNotification object:nil];
}

- (void)bridgeWillReload:(NSNotification *)noti {
  [self.loadedBundles removeAllObjects];
}

#pragma mark -- Public
- (void)applicationStateChangeHandle:(NSString *)state {
    RCTBridge *bridge = self.currentBridge;
    [bridge enqueueJSCall:@"EvgEvent" method:@"emitEvent" args:@[state, @"application"] completion:nil];
}

- (void)tabbarChangeHandle:(NSInteger)index {
    RCTBridge *bridge = self.currentBridge;
    [bridge enqueueJSCall:@"EvgEvent" method:@"emitEvent" args:@[@"onTabChange", @"mainTab", @(index)] completion:nil];
}

#pragma mark -- Private

- (BOOL)getExitBundle:(NSString *)bundle {
  if(self.loadedBundles.count == 0){
    return NO;
  }
  return [self.loadedBundles objectForKey:bundle];
}

- (void)saveBundleForKey:(NSString *)bundle {
  [self.loadedBundles setObject:@(YES) forKey:bundle];
}

- (void)invalidateManager {
  [self.loadedBundles removeAllObjects];
}


#pragma mark -- RCTBridgeDelegate
- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge {
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index"];
}

#pragma mark -- getter & setter
- (NSString *)commonBundleName {
  if (!_commonBundleName) {
    _commonBundleName = @"common";
  }
  return _commonBundleName;
}

- (NSMutableDictionary *)loadedBundles {
  if (!_loadedBundles) {
    _loadedBundles = [NSMutableDictionary dictionary];
  }
  return _loadedBundles;
}

- (RCTBridge *)currentBridge {
  return self.bridge;
}

@end
