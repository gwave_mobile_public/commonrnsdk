//
//  BusinessBundleBridger.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/16.
//

#import "EvgBundleModule.h"
#import "EvgBundleManage.h"
#import "EvgBundleModel.h"

@implementation EvgBundleModule

RCT_EXPORT_MODULE();

RCT_REMAP_METHOD(loadBizModule,
                 loadBizModule:(NSString *)module
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  
  [[EvgBundleManage sharedInstance] getBundleInfo:module bundleInfo:^(EvgBundleModel * _Nullable bundleModel, EvgBundleErrorModel * _Nullable errorModel) {
    if (errorModel) {
      // getbundle info error
      NSInteger code = errorModel.errorCode;
      NSString *errorMsg = errorModel.errorMsg;
      
      reject(@(code).stringValue, errorMsg, nil);
      return;
    }
    if (!bundleModel) {
      reject(@"1000", [NSString stringWithFormat:@"未找到业务模块:%@", module], nil);
      return;
    }
    EvgBundleContentModel *contentModel = [[EvgBundleManage sharedInstance] getBundleContent:bundleModel.bundleUrl.path];
    
    if (contentModel.errorModel) {
      NSInteger code = contentModel.errorModel.errorCode;
      NSString *errorMsg = contentModel.errorModel.errorMsg;
      
      reject(@(code).stringValue, errorMsg, nil);
    } else {
      if (contentModel == nil) {
        reject(@"1001", [NSString stringWithFormat:@"%@:content为空", module], nil);
      } else {
        resolve(@{
                  @"script": contentModel.bundleContent,
                  @"scriptPath":bundleModel.bundleUrl.path,
                  @"version":bundleModel.version
                });
      }
    }
  }];
}

RCT_EXPORT_METHOD(signStable:(NSString *)bundleName  bundleVersion:(NSString *)version)
{
  [[EvgBundleManage sharedInstance] signStableVersion:bundleName bundleVersion:version];
}

RCT_EXPORT_METHOD(signUnstable:(NSString *)bundleName  bundleVersion:(NSString *)version)
{
  [[EvgBundleManage sharedInstance] signUnstableVersion:bundleName bundleVersion:version];
}


@end
