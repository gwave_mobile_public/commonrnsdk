//
//  EvgBridgeManager.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/7.
//

#import <Foundation/Foundation.h>

#if __has_include(<React/RCTBridge+Private.h>)
#import <React/RCTBridge+Private.h>
#else
#import "RCTBridge+Private.h"
#endif

#if __has_include(<React/RCTBundleURLProvider.h>)
#import <React/RCTBundleURLProvider.h>
#else
#import "RCTBundleURLProvider.h"
#endif

#if __has_include(<React/RCTRootView.h>)
#import <React/RCTRootView.h>
#else
#import "RCTRootView.h"
#endif


NS_ASSUME_NONNULL_BEGIN

extern NSString *const EvgBridgeManagerJavaScriptStartNotification;
extern NSString *const EvgBridgeManagerJavaScriptDidLoadNotification;
extern NSString *const EvgBridgeManagerJavaScriptDidFailNotification;
// JavaScript加载后续业务包时，此通知不会收到消息
extern NSString *const EvgBridgeManagerBusinessJavaScriptDidLoadNotification;

typedef void(^SuccessBlock)(void);
typedef void(^FailedBlock)(NSString *msg);

@interface EvgBridgeManager : NSObject<RCTBridgeDelegate>

+ (instancetype)sharedInstance;

- (RCTBridge *)currentBridge;

// 构建bride bundle的url
- (NSURL *)commonSourceUrl;
- (NSURL *)sourceURLWithBundleName:(NSString *)bundleName
                        bundlePath:(NSString *)bundlePath;

// 初始化manager
- (void)initBridgeManagerWith:(RCTBridge *)bridge SuccessHandle:(SuccessBlock)success FailedHandle:(FailedBlock)failed;
- (void)initBridgeManagerWithOptions:(NSDictionary *)launchOptions SuccessHandle:(SuccessBlock)success FailedHandle:(FailedBlock)failed;;
- (void)initBridgeManagerWithBundleUrl:(NSURL *)bundleUrl Options:(NSDictionary *)launchOptions SuccessHandle:(SuccessBlock)success FailedHandle:(FailedBlock)failed;;

// 加载业务bundle
- (void)loadJavaScriptWithBundleUrl:(NSURL*)bundleUrl;
- (void)loadJavaScriptWithBundleUrl:(NSURL*)bundleUrl completed:(nullable void(^)(NSString *url))completed;

// 创建rn视图
- (RCTRootView *)buildRootViewWithModuleName:(NSString *)moduleName;
- (RCTRootView *)buildRootViewWithModuleName:(NSString *)moduleName
                           initialProperties:(NSDictionary *)initialProperties;

// 其他事件
- (void)applicationStateChangeHandle:(NSString *)state;
- (void)tabbarChangeHandle:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
