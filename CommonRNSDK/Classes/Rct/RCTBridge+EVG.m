//
//  RCTBridge+EVG.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/10.
//

#import "RCTBridge+EVG.h"
#import <objc/runtime.h>

@implementation RCTBridge (EVG)

-(NSURL *)businessURL {
    id value = objc_getAssociatedObject(self, _cmd);
    return value;
}
- (void)setBusinessURL:(NSURL *)businessURL {
    objc_setAssociatedObject(self, @selector(businessURL), businessURL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
