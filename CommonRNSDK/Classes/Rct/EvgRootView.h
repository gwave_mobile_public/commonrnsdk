#import <Foundation/Foundation.h>

#if __has_include(<React/RCTRootView.h>)
#import <React/RCTRootView.h>
#else
#import "RCTRootView.h"
#endif


@interface RCTRootView (EvgCategory) // RN私有类 ，这里暴露他的接口

- (NSNumber *)reactTag;

@end
