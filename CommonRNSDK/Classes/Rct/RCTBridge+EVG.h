//
//  RCTBridge+EVG.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/10.
//

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#else
#import "RCTBridge.h"
#endif

NS_ASSUME_NONNULL_BEGIN

@interface RCTBridge (EVG)

@property (nonatomic, readwrite) NSURL *businessURL;

@end

NS_ASSUME_NONNULL_END
