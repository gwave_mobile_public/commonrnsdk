//
//  NavigationBridge.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/11.
//

#import "EvgNavigationModule.h"
#import "EvgRNViewController.h"
#import "UIViewController+Evg.h"
#import "BaseNavagationController.h"
#import "EvgModal.h"

@implementation EvgNavigationModule

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(openBusinessPage:(NSString *)businessName
                  route:(NSString *)route
                  params:(NSDictionary *)params
                  animated:(BOOL)animated)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    EvgRNViewController *vc = [EvgRNViewController reactNativeControllerWithBusinessName:businessName route:route params:params];
    [[UIViewController findNavigationController] pushViewController:vc animated:animated];
  });
}

RCT_EXPORT_METHOD(closeBusinessPage:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *nav  = [UIViewController findNavigationController];
        UIViewController *vc = nav.viewControllers.lastObject;
        // 判断当前controller 是否为根节点
        if ([vc isKindOfClass:BaseViewController.class]) {
            BaseViewController *baseVc = (BaseViewController *)vc;
            if (baseVc.isRootStack) {
                NSLog(@"closeBusinessPage: root stack page should‘t closed directly");
                return;
            }
        }
        // 正常返回
        [nav popViewControllerAnimated:animated];
    });
}

RCT_EXPORT_METHOD(closeBusinessPageToRoot:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *nav  = [UIViewController findNavigationController];
        UIViewController *targetController = nil;
        // 查找当前栈的根节点
        for (UIViewController *vc in nav.viewControllers.reverseObjectEnumerator.allObjects) {
            if ([vc isKindOfClass:BaseViewController.class]) {
                BaseViewController *baseVc = (BaseViewController *)vc;
                if (baseVc.isRootStack) {
                    targetController = baseVc;
                }
            }
        }
        if (targetController) {
            [nav popToViewController:targetController animated:animated];
        } else {
            [nav popToRootViewControllerAnimated:animated];
        }
    });
}

RCT_EXPORT_METHOD(closeBusinessPageByLevel:(NSInteger)level animated:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      UINavigationController *nav  = [UIViewController findNavigationController];
        // 查找当前栈的所有controller集合
        NSMutableArray *currentStack = [NSMutableArray array];
        UIViewController *currentStackRoot = nil;
        for (UIViewController *vc in nav.viewControllers.reverseObjectEnumerator.allObjects) {
            [currentStack insertObject:vc atIndex:0];
            if ([vc isKindOfClass:BaseViewController.class]) {
                BaseViewController *baseVc = (BaseViewController *)vc;
                if (baseVc.isRootStack) {
                    currentStackRoot = baseVc;
                    break;
                }
            }
        }
        // 退出到当前栈的根节点
        if (currentStack.count <= level) {
            NSLog(@"%@", [NSString stringWithFormat:@"closeBusinessPageByLevel, current routes are %ld, close level is %ld ", currentStack.count, level]);
            [nav popToViewController:currentStackRoot animated:animated];
            return;
        }
        //查找对应的controller
        UIViewController *currentRoute = [currentStack objectAtIndex:currentStack.count - level - 1];
        
        [nav popToViewController:currentRoute animated:animated];
    });
}

RCT_EXPORT_METHOD(closeBusinessPageToAddress:(NSString *)address animated:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      UINavigationController *nav  = [UIViewController findNavigationController];
        // 查找当前栈的所有controller集合
        UIViewController *currentRoute = nil;
        for (UIViewController *vc in nav.viewControllers.reverseObjectEnumerator.allObjects) {
            if ([vc isKindOfClass:BaseViewController.class]) {
                BaseViewController *baseVc = (BaseViewController *)vc;
                if ([baseVc.pageAddress isEqualToString:address]) {
                    // 倒序遍历，找到则退出
                    currentRoute = baseVc;
                    break;
                }
                if (baseVc.isRootStack) {
                    // 如果已经到当前栈的root，则不继续找
                    break;
                }
            }
        }

        if (currentRoute == nil) {
            NSLog(@"%@", [NSString stringWithFormat:@"closeBusinessPageByAddress can not found avalide controller by address: %@", address]);
            return;
        }
        
        [nav popToViewController:currentRoute animated:animated];
    });
}

RCT_EXPORT_METHOD(openBusinessStack:(NSString *)businessName
                  route:(NSString *)route
                  params:(NSDictionary *)params
                  animated:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        EvgRNViewController *vc = [EvgRNViewController reactNativeControllerWithBusinessName:businessName route:route params:params];
        vc.isRootStack = YES;
        [[UIViewController findNavigationController] pushViewController:vc animated:animated];
    });
}

RCT_EXPORT_METHOD(closeBusinessStack:(BOOL)animated)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *nav = [UIViewController findNavigationController];
        
        UIViewController *targetController = nil;
        BOOL findNearestRoot = NO;
        
        for (UIViewController *vc in nav.viewControllers.reverseObjectEnumerator.allObjects) {
            if (findNearestRoot) {
                // 找到最近路由栈根节点之前的页面
                targetController = vc;
                break;;
            }
            
            if ([vc isKindOfClass:BaseViewController.class]) {
                BaseViewController *baseVc = (BaseViewController *)vc;
                if (baseVc.isRootStack) {
                    findNearestRoot = YES;
                }
            }
        }
        if (targetController == nil) {
            NSLog(@"closeBusinessStack: not found any root stack");
            return;
        }
        // 关闭当前路由栈
        [nav popToViewController:targetController animated:YES];
    });
}

RCT_EXPORT_METHOD(modalReactPage:(NSString *)businessName
                 route:(NSString *)route
                params:(NSDictionary *)params)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    EvgRNViewController *vc = [EvgRNViewController reactNativeControllerWithBusinessName:businessName route:route params:params];
    vc.view.backgroundColor = [UIColor clearColor];
    [[EvgModal sharedInstance] modalReactPage:vc];
  });
}

RCT_EXPORT_METHOD(dismissModalPage)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EvgModal sharedInstance] dismissModalPage];
    });
}

RCT_EXPORT_METHOD(logout:(NSString *)message)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"rnlogout: %@",message);
    });
}

RCT_EXPORT_METHOD(switchRootPage:(NSDictionary *)params)
{
  dispatch_async(dispatch_get_main_queue(), ^{
      [[NSNotificationCenter defaultCenter] postNotificationName:@"RootViewChangeNotification" object:nil userInfo:params];
  });
}

RCT_EXPORT_METHOD(switchMainTab:(NSDictionary *)params)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TabViewChangeSelected" object:nil userInfo:params];
    });
}

RCT_EXPORT_METHOD(pageBackGesture:(BOOL)isAllow)
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UINavigationController *nav  = [UIViewController findNavigationController];
        if ([nav isKindOfClass:[BaseNavagationController class]]) {
            BaseNavagationController *baseNav = (BaseNavagationController *)nav;
            baseNav.isPopGesture = isAllow;
        }
    });
}

@end
