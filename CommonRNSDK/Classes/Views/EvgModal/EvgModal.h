//
//  EvgModal.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/9/19.
//

#import <UIKit/UIKit.h>
#import "EvgRNViewController.h"
#import "BaseNavagationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EvgModal : UIView

+ (instancetype)sharedInstance;
- (void)modalReactPage:(EvgRNViewController *)reactController;
- (void)dismissModalPage;

@property (nonatomic, assign) BOOL isDisplay;
@property (nonatomic, strong, nullable) EvgRNViewController *reactController;

@end

NS_ASSUME_NONNULL_END
