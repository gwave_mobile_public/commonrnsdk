//
//  EvgModal.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/9/19.
//

#import "EvgModal.h"
#import "UIWindow+Evg.h"
#import "UIViewController+Evg.h"

@interface EvgModal()

// 持有nav 为了让reactView响应viewWillAppear
@property (nonatomic, strong) BaseNavagationController *navigation;
@end

@implementation EvgModal

static id instance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    [self initModalViews];
  }
  return self;
}

- (void)initModalViews {
  self.backgroundColor = [UIColor clearColor];
  self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
}

- (void)modalReactPage:(EvgRNViewController *)reactController {
  if (self.reactController) {
    // 暂时不允许存在多层modal
    return;
  }
  
  // 必须使用导航栏包裹，否则RN无法获取正确的safeArea区域
  BaseNavagationController *nav = [[BaseNavagationController alloc] initWithRootViewController:reactController];
  self.navigation = nav;
  self.reactController = reactController;
  [self addSubview:nav.view];
  
  UIViewController *currentVc = [UIViewController currentViewController];
  if ([currentVc.parentViewController isKindOfClass:UITabBarController.class]) {
      currentVc = currentVc.parentViewController;
  }
  if (currentVc) {
      [currentVc.view addSubview:self];
  } else {
      UIWindow *keyWindow = [UIWindow getKeyWindow];
      [keyWindow addSubview:self];
  }
    self.isDisplay = YES;
}

- (void)dismissModalPage {
    for (UIView *subviews in self.subviews) {
      [subviews removeFromSuperview];
    }
    [self removeFromSuperview];
    self.reactController = nil;
    self.navigation = nil;
    self.isDisplay = NO;
}


@end
