//
//  EvgRNViewController.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/8.
//

#import "EvgRNViewController.h"
#import "EvgBridgeManager.h"
#import "EvgRootView.h"

#define RegisterModuleName @"EvgRnMain"

@interface EvgRNViewController ()

@property (nonatomic, copy) NSString *businessName;
@property (nonatomic, copy) NSString *route;
@property (nonatomic, strong) NSDictionary *params;

@property (nonatomic, strong) RCTRootView *reactView;

@end

@implementation EvgRNViewController

#pragma mark - initial
+ (instancetype)reactNativeControllerWithBusinessName:(NSString *)businessName
                                              route:(NSString *)route
                                             params:(NSDictionary *)params {
  return [[self alloc] initWithBusinessName:businessName route:route params:params];
}

- (instancetype)initWithBusinessName:(NSString *)businessName
                             route:(NSString *)route
                            params:(NSDictionary *)params {
  self = [super init];
  if (self) {
    self.businessName = businessName;
    self.route = route;
    self.params = params;
    self.view.backgroundColor = [UIColor whiteColor];
  }
  return self;
}

#pragma mark - life cycle

- (void)dealloc
{
    self.reactView = nil;
    NSLog(@"EvgRNViewController dealloc");
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:YES animated:animated];
  
  [self reactViewStateChanged:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
//  [self.navigationController setNavigationBarHidden:YES animated:animated];

  [self reactViewStateChanged:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupReactNativeView];
  
//  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bridgeWillReload) name:RCTBridgeWillReloadNotification object:nil];
}

#pragma mark -- public

- (void)reactViewStateChanged:(BOOL)display {
  NSNumber *reactTag = [self.reactView reactTag];
  if (reactTag) {
    RCTBridge *bridge = [EvgBridgeManager sharedInstance].currentBridge;
    NSString *action = display ? @"viewAppear" : @"viewDisappear";
    [bridge enqueueJSCall:@"EvgEvent" method:@"emitEvent" args:@[action,reactTag] completion:nil];
  }
}

- (void)updateReactViewPropParams:(NSDictionary *)params {
    [self.reactView setAppProperties:@{@"params": params}];
}

/**
 * maybe useful in the future
 */
//- (void)bridgeWillReload {
//  // 点击reload刷新时，由于业务包也会被清除，需要提前把rn视图释放掉，然后重新加载
//  dispatch_async(dispatch_get_main_queue(), ^{
//    if (!self.debugMode) {
//      [self.reactView removeFromSuperview];
//      self.reactView = nil;
//    }
//  });
//}

#pragma mark -- private
- (void)setupReactNativeView {
  // base props
  NSDictionary *defaultProps = [self getDefaultInitialProps];
  NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:defaultProps];
  [dict setObject:self.params forKey:@"params"];
  [dict setObject:self.businessName forKey:@"businessName"];
  [dict setObject:self.route forKey:@"entry"];
  
  // 加载业务bundle
  self.reactView = [[EvgBridgeManager sharedInstance] buildRootViewWithModuleName:RegisterModuleName initialProperties:dict];
  self.reactView.frame = self.view.bounds;
  [self.view addSubview:self.reactView];
}

- (NSDictionary *)getDefaultInitialProps {
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:appVersion forKey:@"clientVersion"];
    [dict setObject:@([NSDate date].timeIntervalSince1970) forKey:@"timeStart"];
    [dict setObject:self.address forKey:@"address"];
    return dict.copy;
}


#pragma mark -- getter && setter
- (NSDictionary *)params {
    if (!_params) {
        _params = @{};
    }
    return _params;
}

- (NSString *)businessName {
    if (!_businessName) {
        _businessName = @"";
    }
    return _businessName;
}

- (NSString *)route {
    if (!_route) {
        _route = @"";
    }
    return _route;
}

- (NSString *)address {
    if (!_address) {
        _address = [NSUUID UUID].UUIDString;
    }
    return _address;
}

@end
