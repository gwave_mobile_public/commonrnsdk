//
//  EvgRNViewController.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/8.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EvgRNViewController : BaseViewController

+ (instancetype)reactNativeControllerWithBusinessName:(NSString *)moduleName
                                              route:(NSString *)route
                                             params:(NSDictionary *)params;

- (void)reactViewStateChanged:(BOOL)display;

@property (nonatomic, copy) NSString *address;
- (void)updateReactViewPropParams:(NSDictionary *)params;

@end

NS_ASSUME_NONNULL_END
