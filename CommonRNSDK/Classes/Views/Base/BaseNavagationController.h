//
//  BaseNavagationController.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseNavagationController : UINavigationController<UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property(nonatomic,weak) UIViewController* currentShowVC;
@property (nonatomic, assign) BOOL isPopGesture;// YES：打开侧滑返回

@end

NS_ASSUME_NONNULL_END
