//
//  BaseViewController.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

@property (nonatomic, assign) BOOL isRootStack;
@property (nonatomic, copy) NSString *pageAddress;

@end

NS_ASSUME_NONNULL_END
