//
//  EvgAlert.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/9/11.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
  EvgAlertActionStyleDefault = UIAlertActionStyleDefault,
  EvgAlertActionStyleCancel,
} EvgAlertActionStyle;

@interface EvgAlert : NSObject

+ (void)showAlertWithTitle:(NSString *)title
                   Content:(NSString *)content
                 Confirmed:(void (^ __nullable)(UIAlertAction *action))confirmed;

+ (void)showAlertWithTitle:(NSString *)title
                   Content:(NSString *)content
                 Confirmed:(void (^ __nullable)(UIAlertAction *action))confirmed
                  Canceled:(void (^ __nullable)(UIAlertAction *action))canceled;


- (EvgAlert *)createAlertWithTitle:(NSString *)title Content:(NSString *)content;

- (EvgAlert *)addActionWithTitle:(NSString *)title
                           style:(EvgAlertActionStyle)style
                         handler:(void (^ __nullable)(UIAlertAction *action))handler;

- (void)showCurrentAlert;

@end

NS_ASSUME_NONNULL_END
