//
//  EvgAlert.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/9/11.
//

#define defaultConfirm @"Confirm"
#define defaultCancel @"Cancel"

#import "EvgAlert.h"
#import "UIViewController+Evg.h"

@interface EvgAlert()

@property (nonatomic, strong) UIAlertController *alertController;
@property (nonatomic, readonly) NSArray<UIAlertAction *> *actions;

@end

@implementation EvgAlert

#pragma mark - 配置创建
+ (void)showAlertWithTitle:(NSString *)title
                   Content:(NSString *)content
                 Confirmed:(void (^ __nullable)(UIAlertAction *action))confirmed {
  EvgAlert *evgAlert = [EvgAlert new];
  evgAlert = [evgAlert createAlertWithTitle:title Content:content];
  evgAlert = [evgAlert addActionWithTitle:defaultConfirm
                                    style:EvgAlertActionStyleDefault
                                  handler:confirmed];
  [evgAlert showCurrentAlert];
}

+ (void)showAlertWithTitle:(NSString *)title
                   Content:(NSString *)content
                 Confirmed:(void (^ __nullable)(UIAlertAction *action))confirmed
                  Canceled:(void (^ __nullable)(UIAlertAction *action))canceled {
  EvgAlert *evgAlert = [EvgAlert new];
  evgAlert = [evgAlert createAlertWithTitle:title Content:content];
  evgAlert = [evgAlert addActionWithTitle:defaultConfirm
                                    style:EvgAlertActionStyleDefault
                                  handler:confirmed];
  evgAlert = [evgAlert addActionWithTitle:defaultCancel
                                    style:EvgAlertActionStyleCancel
                                  handler:canceled];
  [evgAlert showCurrentAlert];
}


#pragma mark - 链式创建
- (EvgAlert *)createAlertWithTitle:(NSString *)title Content:(NSString *)content {
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleAlert];
  self.alertController = alert;
  
  return self;
}

- (EvgAlert *)addActionWithTitle:(NSString *)title
                           style:(EvgAlertActionStyle)style
                         handler:(void (^ __nullable)(UIAlertAction *action))handler {
  UIAlertAction *action = [UIAlertAction actionWithTitle:title style:style handler:^(UIAlertAction * _Nonnull action) {
    !handler ?: handler(action);
  }];
  if (self.alertController != nil) {
    [self.alertController addAction:action];
  }
  return self;
}

- (void)showCurrentAlert {
  if (!self.alertController) {
    return;
  }
  [[UIViewController currentViewController] presentViewController:self.alertController animated:YES completion:nil];
}

@end
