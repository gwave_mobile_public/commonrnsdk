//
//  UIViewController+Evg.h
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Evg)

/**
 * AppDelegate.window
 */
+ (UIViewController *)currentViewController;
/**
 * AppDelegate.window
 */
+ (UINavigationController *)currentNavigationController;

/**
 [UIApplication sharedApplication].keyWindow
 */
+(UIViewController *)currentKeyWindowViewController;

/**
  1. return currentViewController.navigation if founded
  2. return rootviewcontroller is kind of navigationController
  3. return nil, key window without any navigationController
 */
+ (UINavigationController *)findNavigationController;

+ (UITabBarController *)findTabbarController;

@end

NS_ASSUME_NONNULL_END
