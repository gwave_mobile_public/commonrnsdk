//
//  UIWindow+Evg.h
//  CommonRNSDK
//
//  Created by 朱志勤 on 2023/10/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIWindow (Evg)

+ (UIWindow *)getKeyWindow;

@end

NS_ASSUME_NONNULL_END
