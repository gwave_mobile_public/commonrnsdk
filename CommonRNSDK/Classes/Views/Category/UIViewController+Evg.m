//
//  UIViewController+Evg.m
//  commonrntemplate
//
//  Created by 朱志勤 on 2023/8/11.
//

#import "UIViewController+Evg.h"
#import "UIWindow+Evg.h"

@implementation UIViewController (Evg)

+ (UIViewController *)findTopmostViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController) {
        return [self findTopmostViewController:rootViewController.presentedViewController];
    } else if ([rootViewController isKindOfClass:[UISplitViewController class]]) {
        UISplitViewController *svc = (UISplitViewController *)rootViewController;
        if (svc.viewControllers.count) {
            return [self findTopmostViewController:svc.viewControllers.lastObject];
        } else {
            return rootViewController;
        }
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navi = (UINavigationController *)rootViewController;
        if (navi.viewControllers.count) {
            return [self findTopmostViewController:[navi.viewControllers lastObject]];
        } else {
            return rootViewController;
        }
    } else if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController;
        if (tabBarController.viewControllers.count) {
            return [self findTopmostViewController:tabBarController.selectedViewController];
        } else {
            return rootViewController;
        }
    } else {
        return rootViewController;
    }
}

+ (UIViewController *)currentViewController
{
    UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    return [self findTopmostViewController:rootViewController];
}

+ (UINavigationController *)currentNavigationController
{
    return [self currentViewController].navigationController;
}

+(UIViewController *)currentKeyWindowViewController
{
    UIWindow *keyWindow = [UIWindow getKeyWindow];
    UIViewController *viewController = keyWindow.rootViewController;
    return [UIViewController findTopmostViewController:viewController];
}

+ (UINavigationController *)findNavigationController {
    UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *currentController = [self currentViewController];
    if ([currentController.navigationController isKindOfClass:[UINavigationController class]] ) {
        return currentController.navigationController;
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        return (UINavigationController *)rootViewController;
    }
    return nil;
}

+ (UITabBarController *)findTabbarController {
    UIWindow *keyWindow = [UIWindow getKeyWindow];
    UIViewController *viewController = keyWindow.rootViewController;
    if ([viewController isKindOfClass:UITabBarController.class]) {
        return (UITabBarController *)viewController;
    }
    if ([viewController isKindOfClass:UINavigationController.class]) {
        UINavigationController *navController = (UINavigationController *)viewController;
        for (UIViewController *controller in navController.viewControllers) {
            if ([controller isKindOfClass:UITabBarController.class]) {
                return (UITabBarController *)controller;
            }
        }
    }
    return nil;
}

@end
