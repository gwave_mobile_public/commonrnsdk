# CommonRNSDK

[![CI Status](https://img.shields.io/travis/yangpeng/CommonRNSDK.svg?style=flat)](https://travis-ci.org/yangpeng/CommonRNSDK)
[![Version](https://img.shields.io/cocoapods/v/CommonRNSDK.svg?style=flat)](https://cocoapods.org/pods/CommonRNSDK)
[![License](https://img.shields.io/cocoapods/l/CommonRNSDK.svg?style=flat)](https://cocoapods.org/pods/CommonRNSDK)
[![Platform](https://img.shields.io/cocoapods/p/CommonRNSDK.svg?style=flat)](https://cocoapods.org/pods/CommonRNSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CommonRNSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CommonRNSDK'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

CommonRNSDK is available under the MIT license. See the LICENSE file for more info.
