#!/bin/bash

#代码编写完成后，可通过此脚本更新podspec，提交，打tag，提交到对应的source源等

#CommonRNSDK版本
commonrnsdk_version="$1"

#CommonRNSDK项目路径
project_path="$2"

if [ ! "$project_path" ] ;then
    root_path="$HOME"
else
    root_path="$HOME/$project_path"
fi

#podspec文件路径
podspec_path="$root_path/CommonRNSDK/CommonRNSDK.podspec"

#更新version字段
sed -i '' "s/s.version *= *'[0-9.]*'/s.version          = '$commonrnsdk_version'/" "$podspec_path"

#查看是否修改成功
cat $podspec_path

#修改代码提交到gitlab
gitlab_repo="git@gitlab.com:gwave_mobile_public/commonrnsdk.git"
local_repo="$root_path/CommonRNSDK"

cd "$local_repo"
git pull origin dev

git add .
git commit -m "Update podspec $commonrnsdk_version"
git push origin dev

#增加tag标签
git tag $commonrnsdk_version
git push origin $commonrnsdk_version

#podspec文件copy一份放到指定目录
source_path="$root_path/cocoapods-kiki-mobile-dev/CommonRNSDK"
source_version_path="$source_path/$commonrnsdk_version"
#创建版本目录文件夹
mkdir "$source_version_path"
#podspec copy 到source_version_path路径
cp -r "$podspec_path" "$source_version_path"

#修改代码提交到gitlab
gitlab_source_repo="git@gitlab.com:gwave_mobile_public/cocoapods-kiki-mobile-dev.git"
local_source_repo="$root_path/cocoapods-kiki-mobile-dev"

cd "$local_source_repo"
git pull origin master

git add .
git commit -m "Add CommonRNSDK $commonrnsdk_version"
git push origin master

#钉钉机器人通知
webhook_url="https://oapi.dingtalk.com/robot/send?access_token=c85363cb324c6e545d5222d9c49051d6db1545a435536d5d9e0756017f56ec99"
publishText="CommonRNSDK $commonrnsdk_version Publish Success"

payload='{
    "msgtype": "link",
    "link": {
        "title": "iOS Pod Publish",
        "text": "'"$publishText"'",
        "messageUrl": "https://gitlab.com/gwave_mobile_public/commonrnsdk/-/tags",
        "picUrl": "https://img1.baidu.com/it/u=776641794,2368154126&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"
    }
}'

curl -H "Content-Type: application/json" -d "${payload}" "${webhook_url}"
